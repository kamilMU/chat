import React from 'react';
import './App.css';

class MessageList extends React.Component {
 
    render() {
        return (
            <form className="form_" onClick={this.props.toggleUser}>
            {this.props.messages.map(mes => 
                <div 
                    key={mes.id} 
                    className="Messages" 
                    userid={this.props.userid}
                    style={{ alignItems: this.props.userID === 1 ? 'flex-end' : 'flex-start' }}> 
                    <div className="head_message">
                        user: {mes.name}  &nbsp;
                        current time: {mes.date}
                    </div>
                {mes.message}
                </div>)}
            </form>
        )
    }
}

export default MessageList;