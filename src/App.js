import React from 'react';
import './App.css';
import MessageInput from './messageInput';
import MessageList from './messageList';

export default class App extends React.Component{
    constructor(props) {
      super(props)
      this.state = {
        messages: [],
        message: "",
        userid: 1
      }
    }

    toggleUser = () => {
      this.setState({ userid: !this.state.userid})
    }

    changeInputText = (e) => {
      this.setState({ message: e.target.value })
    }

    addMessage =(e) => {
      e.preventDefault();
      var date = new Date().getHours();
      var month = new Date().getMinutes();
      var year= new Date().getSeconds();
      let newMessage = {
        id: Date.now(),
        message: this.state.message,
        date: date + ':' + month + ':' + year,
        name: this.state.userid
      };
      const addedMessages = this.state.message !== "" ? [...this.state.messages, newMessage] : this.state.messages;
      this.setState({ messages: addedMessages })
      this.setState({ message: "" })
    }

    render() {
      return (
        <div className="App">
          <header>Chat</header>
          <MessageList 
           messages={this.state.messages} 
           toggleUser={this.toggleUser}
           userid={this.state.userid}
          /> 
          <MessageInput 
           changeInputText={this.changeInputText}
           message={this.state.message}
           addMessage={this.addMessage}
          />
        </div>
      );
    }
}


