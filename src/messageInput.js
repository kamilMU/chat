import React from 'react';

class MessageInput extends React.Component {
    render() {
        return (
            <form className="button_input" onClick={this.addMessage}>
                <input 
                    type="text"
                    autoFocus
                    className="Input"
                    value={this.props.message} 
                    onChange={this.props.changeInputText}
                    placeholder="Add message..."
                    />    
                    <button type="submit" onClick={this.props.addMessage}> Send message </button>
            </form>
        )
    }
}

export default MessageInput;